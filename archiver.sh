#!/bin/sh

tar -zxf termux-backup.tar.gz --preserve-permissions
find ./home -type f -print0 | xargs -0 perl -pi -e 's:com.termux:a.intercom:g'
find ./home -type f -print0 | xargs -0 perl -pi -e 's:xumret.moc:mocretni.a:g'
find ./usr -type f -print0 | xargs -0 perl -pi -e 's:com.termux:a.intercom:g'
find ./usr -type f -print0 | xargs -0 perl -pi -e 's:xumret.moc:mocretni.a:g'
tar -zcf intptyfs.tar.gz ./home ./usr
rm -rf home
rm -rf usr
