#!/data/data/com.termux/files/usr/bin/sh

echo "***** Prepare *****"

termux-setup-storage
pkg install wget
pkg install build-essential
pkg install binutils
pkg install libblocksruntime
pkg install libjansson
pkg install libsqlite
pkg install libedit
pkg install libresolv-wrapper
pkg install termux-services
pkg install libxml2
pkg install libuuid
wget https://gitlab.com/DanKalinin.Android.Termux/packages/-/raw/main/libbthread.tar.gz
wget https://gitlab.com/DanKalinin.Android.Termux/packages/-/raw/main/asterisk-13.38.3.tar.gz
wget https://gitlab.com/DanKalinin.Android.Termux/intercom/-/raw/main/run
wget https://gitlab.com/DanKalinin.Android.Termux/intercom/-/raw/main/res_config_sqlite3.conf
wget https://gitlab.com/DanKalinin.Android.Termux/intercom/-/raw/main/extconfig.conf
wget https://gitlab.com/DanKalinin.Android.Termux/intercom/-/raw/main/sip.conf
wget https://gitlab.com/DanKalinin.Android.Termux/intercom/-/raw/main/extensions.conf

echo "***** Thread *****"

tar -xvzf libbthread.tar.gz
cd libbthread
autoreconf -i
./configure --prefix=$PREFIX
make
make install
cd ..

echo "***** Asterisk *****"

tar -xvzf asterisk-13.38.3.tar.gz
cd asterisk-13.38.3
./configure --prefix=$PREFIX CFLAGS="-DHAVE_FFSLL -DHAVE_GETHOSTBYNAME_R_6 -DHAVE_RES_NINIT" LDFLAGS="-landroid-glob -lbthread -lBlocksRuntime"
make
make install
make samples
cd ..

echo "***** Configuration *****"

mv res_config_sqlite3.conf $PREFIX/etc/asterisk
mv extconfig.conf $PREFIX/etc/asterisk
mv sip.conf $PREFIX/etc/asterisk
mv extensions.conf $PREFIX/etc/asterisk

echo "***** Service *****"

mkdir -p $PREFIX/var/service/asterisk/log
ln -sf $PREFIX/share/termux-services/svlogger $PREFIX/var/service/asterisk/log/run
mv run $PREFIX/var/service/asterisk
chmod +x $PREFIX/var/service/asterisk/run

echo "***** Cleanup *****"

apt purge wget
apt purge build-essential
apt purge binutils
apt autoremove
rm asterisk-13.38.3.tar.gz
rm -rf asterisk-13.38.3
rm libbthread.tar.gz
rm -rf libbthread
rm installer.sh

echo "***** Backup *****"

tar -zcf /sdcard/termux-backup.tar.gz -C /data/data/com.termux/files ./home ./usr
