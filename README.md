## installer.sh

```sh
pkg install wget
wget https://gitlab.com/DanKalinin.Android.Termux/intercom/-/raw/main/installer.sh
chmod +x installer.sh
./installer.sh
```

## archiver.sh

```sh
brew install wget
wget https://gitlab.com/DanKalinin.Android.Termux/intercom/-/raw/main/archiver.sh
chmod +x archiver.sh
./archiver.sh
```
